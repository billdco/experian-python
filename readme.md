# Salesforce Experian API for Build

This application is meant to update SalesForce account objects with data from credit reports in Experian.
We want to save json objects as file attachments to the customer for the required reports, and add 10 (chosen by what seems most relevant)
fields for each report to the customer object so they can view them in the SalesForce UI.

Example api manual run endpoint:
http://prod.fgrhbpyv9y.us-west-2.elasticbeanstalk.com/credit_check/001Q000001HRG7JIAX?key=build-api-key-1

In this url `001Q000001HRG7JIAX` is the account id which can be replaced. The base url is the default beanstalk generated url, we may replace this with a production url in the future.


*Outliant created AWS Credentials:*

User name | Password | Access key ID | Secret access key | Console login link
|---|---|---|---|---|
Outliant | PhKwi%4S*suC | AKIAZHDDQQT3G5NCBAW2 | gRl09zAEp2Caqa9wLsGQLxZsjQfm10atBVZ+pzXS | https://633716376822.signin.aws.amazon.com/console

** Deployment: **
Configure AWS named profile `billd` with credentials (us-west-2 default region) and deploy to beanstalk via `eb deploy prod` in the root directory.
This is done so that you can run AWS CLI from your local desktop in a terminal session.


Step 1:
Go into the repository folder containing all the experian-python files.


Step 2: 
Create a new zip file to be finally loaded into elastic beanstalk

`zip -r app-ba3c-210129_100148-sandbox-1.zip .` 

The naming standard should indicate the environment and a version number.


Step 3: 
The following command will upload the local zip file to the correct S3 bucket for elastic beanstalk will pick it up.

`aws s3 cp app-ba3c-210129_100148-sandbox-1.zip s3://elasticbeanstalk-us-west-2-633716376822/experian-salesforce/app-ba3c-210129_100148-sandbox-1.zip`


Step 4: 
The following command will create a new elastic beanstalk application version using the file from S3.

`aws elasticbeanstalk create-application-version --application-name experian-salesforce --version-label app-ba3c-210129_100148-sandbox-1 --source-bundle S3Bucket="elasticbeanstalk-us-west-2-633716376822",S3Key="experian-salesforce/app-ba3c-210129_100148-sandbox-1.zip"`


Step 5:
The following command will load the new application version into the elastic beanstalk application.

`aws elasticbeanstalk update-environment --application-name experian-salesforce --environment-name sandbox --version-label app-ba3c-210129_100148-sandbox-1`


**Experian Doc:s**

https://developer.experian.com/sbcs/apis/post/v1/search

**SalesForce Docs:**

https://developer.salesforce.com/docs/api-explorer/sobject/Account

**Create Custom Fields in salesforce object:**

https://billdco123--alice.lightning.force.com/lightning/setup/ObjectManager/Account/FieldsAndRelationships/view
https://billdco123.lightning.force.com/lightning/setup/ObjectManager/Account/FieldsAndRelationships/view

## Trello Ticket:
https://trello.com/c/ZSjXnhmr/58-billd-salesforce-experian-api-integration

### Update September 6th

Here are the updates from Chris with supposedly everything we need

**Endpoints & Custom Objects**

- Business Owners API
- Business Owner Profile

_Note:_ Choose 10 fields that we think are most relevant and create these 3 custom objects

- business owner credit report (10 fields)
- business credit report (10 fields)

**Misc**

- Subcode: 0553783

**Ref emails**

- https://app.sparkmailapp.com/web-share/Z0BBCsSOOg34eU2RHHJLNbVffNPXmAUP_XjlaKry
- https://app.sparkmailapp.com/web-share/UvCYnjSvkrYwyIlcpMS5_zCTLTa7neQcLdvkQjfd
Subcode: 0553783

**Salesforce Production Credentials**

- https://billdco123.my.salesforce.com
- rob@alice.design.billd
- [,%%?${,-{{~1q

**AWS Credentials**

- https://portal.aws.amazon.com
- bharat@billdco.com
- u5O\VwqU[Y{fuc%da;>RWH5t5Hh2@B
- f1X=+,O;NolN{Q\7M!Klc.i707Ra{]
- Dave Vennell IAM
- BilldBharat
- f1X=+,O;NolN{Q\7M!Klc.i707Ra{]
- Account: 7519-7279-1512

---

Billd wants to integrate their Salesforce with the Experian API.

Upon contractors completing Enrollment salesforce would automatically send the consumer and business information out to Experian to check their personal and business credit.

We would retrieve about 100 fields that would be placed in fields in salesforce. They'd also like to be able to perform a manual run.

----

**Experian Credentials**

- https://developer.experian.com/
- manasa@billdco.com
- LakeTravis501@

**Salesforce Sandbox**

- https://billdco123--alice.lightning.force.com/lightning/page/home
- rob@alice.design.billd.alice
- [,%%?${,-{{~1q

**AWS Credentials**

- https://portal.aws.amazon.com
- bharat@billdco.com
- u5O\VwqU[Y{fuc%da;>RWH5t5Hh2@B
- f1X=+,O;NolN{Q\7M!Klc.i707Ra{]
- Dave Vennell IAM
- BilldBharat
- f1X=+,O;NolN{Q\7M!Klc.i707Ra{]
- Account: 7519-7279-1512