public class ExperianReportAPI {
    @future (callout=true)
    public static void pullExperianBusinessReport(String accountId) {
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        req.setEndpoint('http://prod.fgrhbpyv9y.us-west-2.elasticbeanstalk.com/credit_check/'+accountId+'?key=build-api-key-1');
        req.setMethod('GET');
        try {
            res = http.send(req);
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
    }

    @future (callout=true)
    public static void pullExperianPersonalReport(String accountId) {
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();

        req.setEndpoint('http://prod.fgrhbpyv9y.us-west-2.elasticbeanstalk.com/personal_credit_check/'+accountId+'?key=build-api-key-1');
        req.setMethod('GET');
        try {
            res = http.send(req);
        } catch(System.CalloutException e) {
            System.debug('Callout error: '+ e);
            System.debug(res.toString());
        }
    }
}
