from flask import abort, Flask, Response, request, jsonify
from flask_sslify import SSLify
from sfexp.coordinator import SFECoordinator

import os
import time
from functools import wraps

app = Flask(__name__)
application = app

coordinator = SFECoordinator()


def require_api_key_arg(func):
    @wraps(func)
    def authenticate_and_call(*args, **kwargs):
        try:
            authorized = request.args.get('key') and request.args.get('key') == "build-api-key-1"
        except Exception as e:
            print(e)
            authorized = False
        if not authorized:
            return jsonify({'status': 'error', 'message': 'invalid auth key'}), 401
        return func(*args, **kwargs)

    return authenticate_and_call


# if os.environ.get('USE_SSL') == 'true':
#     sslify = SSLify(app)


@app.route('/', methods=['GET'])
def root():
    return Response('SFEXP API v-{} by Outliant.'.format("0.1.0"))


@app.route('/health', methods=['GET'])
def health():
    return Response('Ok')


@app.route('/credit_check/<account_id>', methods=['GET'])
@require_api_key_arg
def credit_check(account_id):
    resp = coordinator.get_business_credit_for_account(account_id)
    return jsonify(resp), 200

@app.route('/personal_credit_check/<account_id>', methods=['GET'])
@require_api_key_arg
def personal_credit_check(account_id):
    resp = coordinator.get_personal_credit(account_id)
    return jsonify(resp), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0',
            port=app.config.get("PORT", 7331),
            threaded=True)
