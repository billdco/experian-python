@isTest
private class ExperianReportTriggerTest {
    class HttpMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest request) {
            // Create a fake response.
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody('{"test": "test"}');
            response.setStatusCode(200);
            return response;
        }
    }

    @IsTest
    static void testTrigger() {
        Test.setMock(HttpCalloutMock.class, new HttpMock());

        Account testAccount = new Account(Name = 'Sample Account');
        insert testAccount;

        testAccount.Date_MCA_Executed__c = Date.newInstance(2019, 12, 1);
        update testAccount;
    }
}
