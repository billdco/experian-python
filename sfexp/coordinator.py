from sfexp.salseforce_api import SalesForceAPI
from sfexp.experian_api import ExperianAPI
from sfexp.utils import json_print
import json


class SFECoordinator:
    def __init__(self):
        self.sales_force = SalesForceAPI()
        self.experian = ExperianAPI()

    def get_business_credit_for_account(self, sf_account_id, update_fields=True):
        account = self.sales_force.get_account_by_id(sf_account_id)
        if isinstance(account, list) and len(account):
            return {
                'salesforce_resp': account,
                'account_id': sf_account_id
            }

        # json_print(account)
        # self.handle_account_contact(contact_id)

        # "Experian", "535 ANTON BLVD", "Costa Mesa", "CA", "92626", "9495673800", "176970333"
        name = account.get('Legal_Business_Name__c')
        street = account.get('BillingStreet')
        city = account.get('BillingCity')
        state = account.get('BillingState')
        zip_code = account.get('BillingPostalCode')

        phone = account.get('Phone')
        if phone:
            phone = format_phone(phone)

        tax_id = account.get('Federal_Tax_ID__c', '')
        if tax_id:
            tax_id = tax_id.replace('-', '')

        account_fields = {
            "name": name,
            "street": street,
            "city": city,
            "state": state,
            "zip": zip_code,
            "phone": phone,
            "taxId": tax_id
        }
        print("State:", state)
        search_resp = self.experian.business_search(name, street, city, state, zip_code, phone, tax_id)

        # if not search_resp.get('success'):
        #     return {
        #         'status': 'failed',
        #         'message': 'no business returned in search',
        #         'account_fields': account_fields,
        #         'business_search': search_resp
        #     }

        final_resp = {
            'status': 'success',
            'account_fields': account_fields,
            'business_search': search_resp
        }

        company_results = search_resp.get('results', [])
        if len(company_results):
            bin_id = company_results[0].get('bin')

            premier_profiles = self.experian.premier_profiles(bin_id)
            premier_profile_pdf = self.experian.premier_profile_pdf(bin_id)

            final_resp["premier_profiles"] = premier_profiles

        else:
            final_resp["premier_profiles"] = {}
            premier_profile_pdf = None

        if update_fields:
            resp = self.sales_force.update_exp_business_report(sf_account_id, name, final_resp, premier_profile_pdf)
            if not resp["success"]: return resp

            final_resp["business_report_update_resp"] = resp

            # Flag to indicate that report has already generated for this account.
            # self.sales_force.update_account_fields(sf_account_id, {"credit_report_generated__c": True})

        return final_resp


    def get_personal_credit(self, sf_account_id, update_sf_fields=True):
        account_info = self.sales_force.get_account_by_id(sf_account_id)
        business_owners = self.sales_force.get_business_owners(sf_account_id)

        if len(business_owners):
            final_resp = []
            for owner in business_owners:
                owner_info = self.format_contact_info(owner)
                if not (owner_info["street"] and owner_info["city"] and owner_info["state"] and owner_info["zip"]):
                    owner_info.update({
                        "street": account_info.get("ShippingAddress", {}).get("street"),
                        "city": account_info.get("ShippingAddress", {}).get("city"),
                        "state": account_info.get("ShippingAddress", {}).get("state"),
                        "zip": account_info.get("ShippingAddress", {}).get("postalCode"),
                    })

                business_owner_profile = self.experian.get_business_owner_profile(**owner_info)
                business_owner_profile_pdf = self.experian.get_business_owner_profile_pdf(**owner_info)

                first_name = owner_info.get("first_name") or ""
                last_name = owner_info.get("last_name") or ""
                name = "{} {}".format(first_name, last_name).strip() if (first_name or last_name) else "N/A"

                if update_sf_fields:
                    resp = self.sales_force.update_exp_personal_credit(sf_account_id, name, business_owner_profile, business_owner_profile_pdf)
                    final_resp.append(resp)

            return {
                "num_business_owners": len(business_owners),
                "resp": final_resp
            }

        return {
            "status": "erorr",
            "message": "Business owner not found in SalesForce."
        }


    def format_contact_info(self, contact_info):
        return {
            "last_name": contact_info.get("LastName"),
            "first_name": contact_info.get("FirstName"),
            "middle_name": contact_info.get("MiddleName"),
            "suffix": contact_info.get("Suffix"),
            "street": contact_info.get("MailingStreet"),
            "city": contact_info.get("MailingCity"),
            "state": contact_info.get("MailingState"),
            "zip": contact_info.get("MailingPostalCode"),
            "ssn": contact_info.get("Social_Security_Number__c"),
            "dob": contact_info.get("DOB__c"),
            "role": contact_info.get("Role__c"),
        }

    def generate_credit_report_for_all_accounts(self, new_only=True, limit=None, dry_run=False):
        """
        Long running process (should be run asynchronously).
        """
        resp = self.sales_force.get_all_accounts(new_only=new_only, limit=limit)
        account_ids = map(lambda record: record.get("Id"), resp.get("records", []))

        for account_id in account_ids:
            print("\nGenerating Credit Report for: {}".format(account_id))
            json_print(self.get_business_credit_for_account(account_id, update_fields=not dry_run))


def format_phone(phone_number):
    return phone_number.strip().replace('(', '').replace(')', '').replace(' ', '').replace('-', '')


if __name__ == '__main__':
    ctr = SFECoordinator()

    resp = ctr.get_business_credit_for_account("0015G00001Y5E5LQAV")
    # resp = ctr.get_personal_credit("001f400001GZXVVAA5")
    contact_fields = {
        "last_name": "MANTIA",
        "first_name": "BERTHA",
        "middle_name": "M",
        "suffix": "GEN",
        "street": "7535 N  CATLIN  AVE",
        "city": "PORTLAND",
        "state": "OR",
        "zip": "972032144",
        "ssn": "666184416",
        "dob": None,
        "role": None
    }
    # json_print(contact_fields)
    # resp = ctr.experian.business_owner_report(**contact_fields)
    json_print(resp)
