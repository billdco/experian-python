import base64
import json
import requests
import time
from sfexp.utils import json_print
from datetime import datetime
from dateutil.relativedelta import relativedelta

from urllib.parse import quote_plus


import boto3
import os

class SalesForceConfig:
    def __init__(self):
        # Prod:
        self.security_token = "2DZUG1qmdV9L8RCDttbllqQW"
        self.client_id = "3MVG9zlTNB8o8BA21timiVqPbQfFxp2GVy8oqG6DvXzPHPUDT84SrJD5OTZFrXrRD7q_RrvKzi8INEEQ0HEIR"
        self.client_secret = "3A09BAE355ABC438D072F848943768D98ADAC5B07B8D1049F20BBF9AC34D5D47"
        self.user_name = "dev@billd.com"
        self.password = "LakeAustin512@"
        self.auth_url = "https://login.salesforce.com/services/oauth2/token"

        # Sandbox
        self.security_token = "afxqhvnXKLBqCGiEkZjLdc61"
        self.client_id = "3MVG9pHRjzOBdkd9a7QJNoa2p5VwZZgklLlTOLIGJAj_Z26GHBUc_bTU5vBkOxDdHJzIHVbBe1NPqoDRMPGuB"
        self.client_secret = "2016F80753ED3C687C38EE0665C8066ACE40B29433975D77A5CE3BE76E1C31FC"
        self.user_name = "joel@outliant.com"
        self.password = "D2FsV43zCkPPq6b"
        self.auth_url = "https://test.salesforce.com/services/oauth2/token"


class SalesForceAPI:
    def __init__(self):
        self.access_token = None

        self.base_url = None
        self.default_headers = None

        self.authenticated_at = None
        self.token_expiry = 300 # estimated auth token expiry time (in seconds).

        # TODO: Only authenticate when expired, instead of on each request?
        # self.authenticate()

    def authenticate(self):
        # https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/quickstart_oauth.htm
        if self.authenticated_at and self.token_expiry > (time.time() - self.authenticated_at):
            return

        self.set_sf_credentials()

        data = {
            "grant_type": "password",
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "username": self.user_name,
            "password": self.password + self.security_token
        }
        resp = requests.post(self.auth_url, data=data)
        print(data)
        print(resp.text)
        resp_json = resp.json()
        print("Authenticate", resp_json)
        self.access_token = resp_json.get('access_token')
        self.base_url = resp_json.get('instance_url')
        self.default_headers = {
            "Authorization": "Bearer {session_id}".format(session_id=self.access_token),
            "Content-Type": "application/json"
        }

        self.authenticated_at = time.time()

    def get_contact_by_id(self, contact_id):
        self.authenticate()
        url = "{}/services/data/v39.0/sobjects/Contact/{}".format(self.base_url, contact_id)

        resp = requests.get(url, headers=self.default_headers)
        return resp.json()

    def get_account_by_id(self, account_id):
        self.authenticate()
        url = "{}/services/data/v39.0/sobjects/Account/{}".format(self.base_url, account_id)

        resp = requests.get(url, headers=self.default_headers)
        return resp.json()

    # Custom fields must be created here:
    # https://billdco123--alice.lightning.force.com/lightning/setup/ObjectManager/Account/FieldsAndRelationships/view
    def update_account_fields(self, account_id, fields):
        self.authenticate()
        # https://developer.salesforce.com/docs/api-explorer/sobject/Account/patch-account-id
        url = "{}/services/data/v39.0/sobjects/Account/{}".format(self.base_url, account_id)
        resp = requests.patch(url, headers=self.default_headers, json=fields)
        print(resp.text)
        return {'resp': resp.text}

    def get_account_contact_role_summary(self):
        self.authenticate()
        url = "{}/services/data/v39.0/sobjects/AccountContactRole".format(self.base_url)
        resp = requests.get(url, headers=self.default_headers)
        return resp.json()

    def attach_object(self, account_id, attachment_name, json):
        self.authenticate()
        # https://developer.salesforce.com/docs/api-explorer/sobject/Attachment
        url = "{}/services/data/v39.0/sobjects/Attachment".format(self.base_url)
        # files = {
        #     "experian_output": StringIO(json)
        # }
        # resp = requests.post(url, headers=self.default_headers, files=files)
        data = {
            "ParentId": account_id,
            "Name": attachment_name,
            "Body": base64.b64encode(json).decode()
        }
        resp = requests.post(url, headers=self.default_headers, json=data)
        return resp.json()

    def get_attachment(self, attachment_id):
        self.authenticate()
        # https://developer.salesforce.com/docs/api-explorer/sobject/Attachment/get-attachment-id
        url = "{}/services/data/v39.0/sobjects/Attachment/{}".format(self.base_url, attachment_id)
        resp = requests.get(url, headers=self.default_headers)
        return resp.json()


    def attach_file(self, account_id, title, filename, content):
        """
        Attaches file to Account object.
        """
        self.authenticate()

        def create_content_version(title, filename, content):
            # https://developer.salesforce.com/docs/api-explorer/sobject/ContentVersion
            url = "{}/services/data/v39.0/sobjects/ContentVersion".format(self.base_url)
            data = {
                "Title": title,
                "PathOnClient": filename,
                "VersionData": content,
            }
            return requests.post(url, headers=self.default_headers, json=data)

        def get_document_by_version_id(content_version_id):
            # https://developer.salesforce.com/docs/api-explorer/sobject/ContentVersion/get-contentversion-id
            url = "{}/services/data/v39.0/sobjects/ContentVersion/{}".format(self.base_url, content_version_id)
            return requests.get(url, headers=self.default_headers)

        def create_content_document_link(account_id, content_document_id):
            # https://developer.salesforce.com/docs/api-explorer/sobject/ContentDocumentLink
            url = "{}/services/data/v39.0/sobjects/ContentDocumentLink".format(self.base_url)
            data = {
                "LinkedEntityId": account_id,
                "ContentDocumentId": content_document_id,
                "ShareType": "V"
            }
            return requests.post(url, headers=self.default_headers, json=data)

        # Creating a ContentVersion object automatically creates a ContentDocument object.
        resp = create_content_version(title, filename, content)
        if resp.ok:
            # Get ContentDocumentId by ContentVersionId.
            content_version_id = resp.json()["id"]
            resp = get_document_by_version_id(content_version_id)
            if resp.ok:
                # Link ContentDocument object to Account object.
                content_document_id = resp.json()["ContentDocumentId"]
                resp = create_content_document_link(account_id, content_document_id)
                return resp.json()
            else:
                return resp.json()
        else:
            return resp.json()

    def check_file_exists(self, account_id, title):
        # https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/dome_query.htm
        self.authenticate()

        soql = "SELECT+Id+FROM+ContentDocumentLink+WHERE+LinkedEntityId='{}'+AND+ContentDocument.Title='{}'".format(account_id, title)
        url = "{}/services/data/v39.0/query/?q={}".format(self.base_url, soql)

        resp = requests.get(url, headers=self.default_headers)
        num_files = resp.json().get("totalSize", 0)

        return num_files > 0

    def get_contact_ids_by_account_id(self, account_id, limit=None):
        self.authenticate()

        url = "{}/services/data/v39.0/query/?q=SELECT+Id+FROM+Contact+WHERE+AccountId='{}'".format(self.base_url, account_id)
        if limit is not None:
            url += "+LIMIT+{}".format(limit)

        resp = requests.get(url, headers=self.default_headers)
        return resp.json()

    def update_exp_business_report(self, account_id, account_name, business_report, premier_profile_pdf):
        premier_profiles = business_report.get("premier_profiles")
        results = premier_profiles.get("results") or {}

        business_header = results.get("businessHeader") or {}

        score_information = results.get("scoreInformation") or {}
        commercial_score = score_information.get("commercialScore") or {}
        commercial_score_trends = { trend.get("quarter"): trend.get("score") for trend in (score_information.get("commercialScoreTrends") or []) }

        frs_score = score_information.get("fsrScore") or {}
        frs_score_trends = { trend.get("quarter"): trend.get("score") for trend in (score_information.get("fsrScoreTrends") or []) }

        expanded_credit_summary = results.get("expandedCreditSummary") or {}
        executive_summary = results.get("executiveSummary") or {}
        sic_codes = results.get("sicCodes") or []

        business_facts = results.get("businessFacts") or {}

        inquiries = [inquiry for category in (results.get("inquiries") or []) for inquiry in (category.get("inquiryCount") or [])]
        #inquiryCount = sum(map(lambda inquiry: inquiry.get("count") or 0, inquiries))
    
        inquiryCount = 0

        inquiries = results.get('inquiries')

        if inquiries is not None:

            #******Begin counting up the inquiries of type TOTALS
            for k in range(len(inquiries)):
                inquiry = inquiries[k]
                if inquiry['inquiryBusinessCategory'] == 'TOTALS':
                    inquiry_items = inquiry['inquiryCount']
                    for l in range(6):
                        inquiry_item = inquiry_items[l]
                        inquiryCount = inquiryCount + inquiry_item['count']

            print('Inquiry items total count: ' + str(inquiryCount))
        #******End counting up the inquiries of type TOTALS
        
        corporate_registration = results.get("corporateRegistration") or {}

        payment_totals = results.get("paymentTotals") or {}
        newly_reported_tradelines = payment_totals.get("newlyReportedTradelines") or {}
        continuously_reported_tradelines = payment_totals.get("continuouslyReportedTradelines") or {}
        combined_tradelines = payment_totals.get("combinedTradelines") or {}
        additional_tradelines = payment_totals.get("additionalTradelines") or {}
        tradelines = payment_totals.get("tradelines") or {}

        # TODO: add a way to map all objects in the list, for now only the first object (if any) is mapped.
        ucc_filings = (results.get("uccFilingsSummary") or {}).get("uccFilingsTrends") or []
        ucc_filing = ucc_filings[0] if len(ucc_filings) else {}

        economic_diversity = results.get("economicDiversity") or {}

        enhanced_business_desc = results.get("enhancedBusinessDescription")
        enhanced_business_desc = enhanced_business_desc.get("description") if isinstance(enhanced_business_desc, dict) else enhanced_business_desc

        body = {
            "AccountId__c": account_id,
            "Success__c": premier_profiles.get("success"),
            "Name": "{} - Business Report ({})".format(account_name, datetime.utcnow().strftime("%Y%m%d-%H%M%S")),
            "Request_ID__c": premier_profiles.get("requestId"),

            "BIN__c": business_header.get("bin") or "null",
            "Business_Name__c": business_header.get("businessName") or "null",
            "Business_Phone__c": business_header.get("phone") or "null",
            "Business_Address__c": format_address(business_header.get("address")) or "null",
            "Tax_ID__c": business_header.get("taxId") or "null",
            "Legal_Business_Name__c": business_header.get("legalBusinessName") or "null",
            "DBA__c": (", ".join(business_header.get("dbaNames")) if business_header.get("dbaNames") else None) or "null",

            "Experian_Business_Commercial_Score__c": commercial_score.get("score") or "null",
            "Experian_Commercial_Percentile_Ranking__c": commercial_score.get("percentileRanking") or "null",
            "Experian_Recommended_Credit_Limit__c": commercial_score.get("recommendedCreditLimitAmount") or "null",
            "Experian_Risk_Class__c": (commercial_score.get("riskClass") or {}).get("definition") or "null",
            "Business_Score_Factors__c": ", ".join(map(lambda x: x.get("definition"), filter(lambda x: x.get("definition") is not None, score_information.get("commercialScoreFactors") or []))) or "null",

            "Experian_FRS_Score__c": frs_score.get("score") or "null",
            "Experian_FRS_Percentile_Ranking__c": frs_score.get("percentileRanking") or "null",

            "Trend_Q1_Commercial_Score__c": commercial_score_trends.get("JAN-MAR") or "null",
            "Trend_Q2_Commercial_Score__c": commercial_score_trends.get("APR-JUN") or "null",
            "Trend_Q3_Commercial_Score__c": commercial_score_trends.get("JUL-SEP") or "null",
            "Trend_Q4_Commercial_Score__c": commercial_score_trends.get("OCT-DEC") or "null",

            "Trend_Q1_FRS_Score__c": frs_score_trends.get("JAN-MAR") or "null",
            "Trend_Q2_FRS_Score__c": frs_score_trends.get("APR-JUN") or "null",
            "Trend_Q3_FRS_Score__c": frs_score_trends.get("JUL-SEP") or "null",
            "Trend_Q4_FRS_Score__c": frs_score_trends.get("OCT-DEC") or "null",

            "Number_of_Tradelines__c": expanded_credit_summary.get("allTradelineCount") or 0,
            "Primary_SIC__c": ", ".join(map(lambda x: x.get("code") or "null", sic_codes)) or "null",
            "Primary_SIC_Description__c": ", ".join(map(lambda x: x.get("definition") or "null", sic_codes)) or "null",
            "Highest_DBT_per_6_months__c": expanded_credit_summary.get("highestDbt6Months") or 0,
            "Highest_DBT_per_5_quarters__c": expanded_credit_summary.get("highestDbt5Quarters") or 0,
            "Business_Bankruptcy_Filings__c": expanded_credit_summary.get("bankruptcyCount") or 0,
            "Business_Tax_Lien_Filings__c": expanded_credit_summary.get("taxLienCount") or 0,
            "Business_Judgement_Filings__c": expanded_credit_summary.get("judgmentCount") or 0,
            "Total_Collections__c": expanded_credit_summary.get("collectionCount") or 0,
            "Business_UCC_Filings__c": expanded_credit_summary.get("uccFilings") or 0,

            "Business_High_Credit_Balance__c": (executive_summary.get("highestTotalAccountBalance") or {}).get("amount") or 0,
            "Business_Curent_Total_Balance__c": (executive_summary.get("currentTotalAccountBalance") or {}).get("amount") or 0,
            "Business_Inquiries_Last_6_months__c": inquiryCount or 0,
            "Business_Total_Collections__c": expanded_credit_summary.get("collectionBalance") or 0,
            "Business_Years_on_File__c": years_ago(business_facts.get("fileEstablishedDate")) if business_facts.get("fileEstablishedDate") else 0,
            "Business_Score_Type__c": commercial_score.get("modelTitle") or "null",

            "Business_Website__c": business_header.get("websiteUrl") or "null",
            # "Business_Items__c": items,
            "Customer_Dispute_Indicator__c": business_header.get("customerDisputeIndicator") if business_header.get("customerDisputeIndicator") is not None else "null",
            "Enhanced_Description__c": enhanced_business_desc or "null",
            "Business_File_Established_Date__c": business_facts.get("fileEstablishedDate") or "null",

            "Business_File_Established_Flag_Code__c": (business_facts.get("yearsInBusinessIndicator") or {}).get("code") or "null",
            "Business_File_Established_Definition__c": (business_facts.get("yearsInBusinessIndicator") or {}).get("definition") or "null",
            "Business_File_State_of_Incorporation__c": business_facts.get("stateOfIncorporation") or "null",
            "Business_File_Date_of_Incorporation__c": business_facts.get("dateOfIncorporation") or "null",
            "Business_Type__c": business_facts.get("businessType") or "null",
            "Years_in_Business_Indicator_Code__c": (business_facts.get("yearsInBusinessIndicator") or {}).get("code") or "null",

            "Years_in_Business__c": business_facts.get("yearsInBusiness") or 0,
            "Business_Sales_Size_Code__c": business_facts.get("salesSizeCode") or "null",
            "Business_Sales_Revenue__c": business_facts.get("salesRevenue") or 0,
            "Business_Employee_Size_Code__c": business_facts.get("employeeSizeCode") or "null",
            "Business_Employee_Size__c": business_facts.get("employeeSize") or 0,
            "Business_Location_Employee_Size_Code__c": business_facts.get("locationEmployeeSizeCode") or "null",
            "Business_Facts_Public_Indicator__c": business_facts.get("publicIndicator") if business_facts.get("publicIndicator") is not None else "null",
            "Business_Facts_Non_Profit_Indicator__c": business_facts.get("nonProfitIndicator") if business_facts.get("nonProfitIndicator") is not None else "null",

            "Corporate_State_of_Origin__c": corporate_registration.get("stateOfOrigin") or "null",
            "Corporate_Original_Filing_Date__c": corporate_registration.get("originalFilingDate") or "null",

            "Corporate_Recent_Filing_Date__c": corporate_registration.get("recentFilingDate") or "null",
            "Corporate_Registration_Incorporated_Date__c": corporate_registration.get("incorporatedDate") or "null",
            "Corporate_Business_Type__c": corporate_registration.get("businessType") or "null",
            "Corporate_Status_Flag__c": (corporate_registration.get("statusFlag") or {}).get("definition") or "null",
            "Corporate_Status_Description__c": corporate_registration.get("statusDescription") or "null",
            "Corporate_Profit_Flag__c": corporate_registration.get("profitFlag") or "null",
            "Corporate_Charter_Number__c": corporate_registration.get("charterNumber") or "null",
            "Corporate_Existence_Term_Years__c": corporate_registration.get("existenceTermYears") or 0,
            "Corporate_Existence_Term_Date__c": corporate_registration.get("existenceTermDate") or "null",
            "Corporate_Federal_Tax_ID__c": corporate_registration.get("federalTaxId") or "null",
            "Corporate_State_Tax_ID__c": corporate_registration.get("stateTaxId") or "null",
            "Corporate_Domestic_Foreign_Indicator__c": corporate_registration.get("domesticForeignIndicator") or "null",
            "Corporate_Agent_Name__c": corporate_registration.get("agentName") or "null",

            "Corporate_Agent_Street__c": (corporate_registration.get("agentAddress") or {}).get("street") or "null",
            "Corporate_Agent_City__c": (corporate_registration.get("agentAddress") or {}).get("city") or "null",
            "Corporate_Agent_State__c": (corporate_registration.get("agentAddress") or {}).get("state") or "null",
            "Corporate_Agent_Zip__c": (corporate_registration.get("agentAddress") or {}).get("zip") or "null",

            "Oldest_Bankruptcy_Date__c": expanded_credit_summary.get("oldestBankruptcyDate") or "null",
            "Most_Recent_Bankruptcy_Date__c": expanded_credit_summary.get("mostRecentBankruptcyDate") or "null",
            "Tax_Lien_Count__c": expanded_credit_summary.get("taxLienCount") or 0,
            "Oldest_Tax_Lien_Date__c": expanded_credit_summary.get("oldestTaxLienDate") or "null",
            "Most_Recent_Tax_Lien_Date__c": expanded_credit_summary.get("mostRecentTaxLienDate") or "null",
            "Judgement_Count__c": expanded_credit_summary.get("judgmentCount") or 0,
            "Oldest_Judgment_Date__c": expanded_credit_summary.get("oldestJudgmentDate") or "null",
            "Most_Recent_Judgment_Date__c": expanded_credit_summary.get("mostRecentJudgmentDate") or "null",
            "Collection_Count__c": expanded_credit_summary.get("collectionCount") or 0,
            "Collection_Balance__c": expanded_credit_summary.get("collectionBalance") or 0,
            "Collection_Count_Past_24_Months__c": expanded_credit_summary.get("collectionCountPast24Months") or 0,
            "Legal_Balance__c": expanded_credit_summary.get("legalBalance") or 0,
            "UCC_Filings__c": expanded_credit_summary.get("uccFilings") or 0,
            "UCC_Derogatory_Count__c": expanded_credit_summary.get("uccDerogatoryCount") or 0,
            "Current_Account_Balance__c": expanded_credit_summary.get("currentAccountBalance") or 0,
            "Current_Tradeline_Count__c": expanded_credit_summary.get("currentTradelineCount") or 0,
            "Monthly_Average_Dbt__c": expanded_credit_summary.get("monthlyAverageDbt") or 0,

            "Active_Tradeline_Count__c": expanded_credit_summary.get("activeTradelineCount") or 0,
            "All_Tradeline_Balance__c": expanded_credit_summary.get("allTradelineBalance") or 0,
            "All_Tradeline_Count__c": expanded_credit_summary.get("allTradelineCount") or 0,
            "Average_Balance_5_Quarters__c": expanded_credit_summary.get("averageBalance5Quarters") or 0,
            "Single_High_Credit__c": expanded_credit_summary.get("singleHighCredit") or 0,
            "Low_Balance_6_Months__c": expanded_credit_summary.get("lowBalance6Months") or 0,

            "High_Balance_6_Months__c": expanded_credit_summary.get("highBalance6Months") or 0,
            "Oldest_Collection_Date__c": expanded_credit_summary.get("oldestCollectionDate") or "null",

            "Most_Recent_Collection_Date__c": expanded_credit_summary.get("mostRecentCollectionDate") or "null",
            "Current_Dbt__c": expanded_credit_summary.get("currentDbt") or 0,
            "Oldest_UCC_Date__c": expanded_credit_summary.get("oldestUccDate") or "null",
            "Most_Recent_UCC_Date__c": expanded_credit_summary.get("mostRecentUccDate") or "null",
            "Bankruptcy_Indicator__c": expanded_credit_summary.get("bankruptcyIndicator") if expanded_credit_summary.get("bankruptcyIndicator") is not None else "null",
            "Judgment_Indicator__c": expanded_credit_summary.get("judgmentIndicator") if expanded_credit_summary.get("judgmentIndicator") is not None else "null",
            "Tax_Lien_Indicator__c": expanded_credit_summary.get("taxLienIndicator") if expanded_credit_summary.get("taxLienIndicator") is not None else "null",
            "Trade_Collection_Count__c": expanded_credit_summary.get("tradeCollectionCount") or 0,
            "Trade_Collection_Balance__c": expanded_credit_summary.get("tradeCollectionBalance") or 0,
            "Open_Collection_Count__c": expanded_credit_summary.get("openCollectionCount") or 0,
            "Open_Collection_Balance__c": expanded_credit_summary.get("openCollectionBalance") or 0,

            "Newly_Reported_Tradeline_Count__c": newly_reported_tradelines.get("numberOfLines") or 0,
            "Newly_Reported_Current_Dbt__c": newly_reported_tradelines.get("dbt") or 0,
            "Newly_Reported_Total_High_Credit_Amount__c": (newly_reported_tradelines.get("totalHighCreditAmount") or {}).get("amount") or 0,
            "Newly_Reported_Total_Account_Balance__c": (newly_reported_tradelines.get("totalAccountBalance") or {}).get("amount") or 0,
            "Newly_Reported_Current_Percentage__c": newly_reported_tradelines.get("currentPercentage") or 0,
            "Newly_Reported_Dbt_30__c": newly_reported_tradelines.get("dbt30") or 0,
            "Newly_Reported_Dbt_60__c": newly_reported_tradelines.get("dbt60") or 0,
            "Newly_Reported_Dbt_90__c": newly_reported_tradelines.get("dbt90") or 0,
            "Newly_Reported_Dbt_91_Plus__c": newly_reported_tradelines.get("dbt91Plus") or 0,

            "Continuously_Reported_Tradeline_Count__c": continuously_reported_tradelines.get("numberOfLines") or 0,
            "Continuously_Reported_Current_Dbt__c": continuously_reported_tradelines.get("dbt") or 0,
            "Continuously_Reported_Total_High_Credit__c": (continuously_reported_tradelines.get("totalHighCreditAmount") or {}).get("amount") or 0,
            "Continuously_Reported_Total_Account_Bal__c": (continuously_reported_tradelines.get("totalAccountBalance") or {}).get("amount") or 0,
            "Continuously_Reported_Current_Percentage__c": continuously_reported_tradelines.get("currentPercentage") or 0,
            "Continuously_Reported_Dbt_30__c": continuously_reported_tradelines.get("dbt30") or 0,
            "Continuously_Reported_Dbt_60__c": continuously_reported_tradelines.get("dbt60") or 0,
            "Continuously_Reported_Dbt_90__c": continuously_reported_tradelines.get("dbt90") or 0,
            "Continuously_Reported_Dbt_91_Plus__c": continuously_reported_tradelines.get("dbt91Plus") or 0,

            "Combined_Tradeline_Count__c": combined_tradelines.get("numberOfLines") or 0,
            "Combined_Tradeline_Current_Dbt__c": combined_tradelines.get("dbt") or 0,
            "Combined_Total_High_Credit_Amount__c": (combined_tradelines.get("totalHighCreditAmount") or {}).get("amount") or 0,
            "Combined_Tradeline_Total_account_Balance__c": (combined_tradelines.get("totalAccountBalance") or {}).get("amount") or 0,
            "Combined_Tradeline_Current_Percentage__c": combined_tradelines.get("currentPercentage") or 0,
            "Combined_Tradeline_Dbt_30__c": combined_tradelines.get("dbt30") or 0,
            "Combined_Tradeline_Dbt_60__c": combined_tradelines.get("dbt60") or 0,
            "Combined_Tradeline_Dbt_90__c": combined_tradelines.get("dbt90") or 0,
            "Combined_Tradeline_Dbt_91_Plus__c": combined_tradelines.get("dbt91Plus") or 0,

            "Additional_Tradeline_Count__c": additional_tradelines.get("numberOfLines") or 0,
            "Additional_Tradeline_Current_dbt__c": additional_tradelines.get("dbt") or 0,
            "Additional_Tradeline_Total_High_Credit__c": (additional_tradelines.get("totalHighCreditAmount") or {}).get("amount") or 0,
            "Additonal_Tradeline_Total_Account_Bal__c": (additional_tradelines.get("totalAccountBalance") or {}).get("amount") or 0,
            "Additional_Tradeline_Current_Percentage__c": additional_tradelines.get("currentPercentage") or 0,
            "Additonal_Tradeline_Dbt_30__c": additional_tradelines.get("dbt30") or 0,
            "Additonal_Tradeline_Dbt_60__c": additional_tradelines.get("dbt60") or 0,
            "Additonal_Tradeline_Dbt_90__c": additional_tradelines.get("dbt90") or 0,
            "Additonal_Tradeline_Dbt_91_Plus__c": additional_tradelines.get("dbt91Plus") or 0,

            "Tradeline_Count__c": tradelines.get("numberOfLines") or 0,
            "Tradeline_Current_Dbt__c": tradelines.get("dbt") or 0,
            "Tradeline_Total_high_Credit_Amount__c": (tradelines.get("totalHighCreditAmount") or {}).get("amount") or 0,
            "Tradeline_Total_Account_Balance__c": (tradelines.get("totalAccountBalance") or {}).get("amount") or 0,
            "Tradeline_Current_Percentage__c": tradelines.get("currentPercentage") or 0,
            "Tradeline_Dbt_30__c": tradelines.get("dbt30") or 0,
            "Tradeline_Dbt_60__c": tradelines.get("dbt60") or 0,
            "Tradeline_Dbt_90__c": tradelines.get("dbt90") or 0,
            "Tradeline_Dbt_91_Plus__c": tradelines.get("dbt91Plus") or 0,

            "UCC_Filings_Summary_Date__c": ucc_filing.get("date") or "null",
            "UCC_Filings_Count__c": ucc_filing.get("count") or 0,
            "UCC_Filings_Derogatory_Count__c": ucc_filing.get("derogatoryCount") or 0,
            "UCC_Releases_Terminations_Count__c": ucc_filing.get("releasesAndTerminationsCount") or 0,
            "UCC_Filings_Continuations_Count__c": ucc_filing.get("continuationsCount") or 0,
            "UCC_Filings_Amended_Assigned_Count__c": ucc_filing.get("amendedAndAssignedCount") or 0,

            "EconomicDiversity_MinorityOwnedIndicator__c": economic_diversity.get("minorityOwnedIndicator") if economic_diversity.get("minorityOwnedIndicator") is not None else "null",
            "EconomicDiversity_WomenOwnedIndicator__c": economic_diversity.get("womenOwnedIndicator") if economic_diversity.get("womenOwnedIndicator") is not None else "null",
            "EconomicDiversity_DisadvantagedIndicator__c": economic_diversity.get("disadvantagedIndicator") if economic_diversity.get("disadvantagedIndicator") is not None else "null",
            "EconomicDiversity_SBACertified_Indicator__c": economic_diversity.get("sbaCertifiedIndicator") if economic_diversity.get("sbaCertifiedIndicator") is not None else "null",
            "EconomicDiversity_SBA8a_Indicator__c": economic_diversity.get("sba8aIndicator") if economic_diversity.get("sba8aIndicator") is not None else "null",
            "EconomicDiversity_Hub_Zone_Indicator__c": economic_diversity.get("hubZoneIndicator") if economic_diversity.get("hubZoneIndicator") is not None else "null",
            "EconomicDiversity_Veteran_OwnedIndicator__c": economic_diversity.get("veteranOwnedIndicator") if economic_diversity.get("veteranOwnedIndicator") is not None else "null",
            "EconomicDiversity_DisableVeteranOwner__c": economic_diversity.get("disabledVeteranOwnedIndicator") if economic_diversity.get("disabledVeteranOwnedIndicator") is not None else "null",
            "Historical_Black_College_Universities__c": economic_diversity.get("historicalBlackCollegeAndUniversitiesIndicator") if economic_diversity.get("historicalBlackCollegeAndUniversitiesIndicator") is not None else "null",
        }
        # body = { key: (value if value is not None else "null") for key, value in body.items() }

        url = "{}/services/data/v39.0/sobjects/Experian_Business_Report__c".format(self.base_url)
        resp = requests.post(url, headers=self.default_headers, json=body)
        if resp.ok:
            report_id = resp.json()["id"]
            self.attach_file(report_id, "business_report.json", "business_report.json",
                             self.b64encode(json.dumps(business_report, indent=4)))

            if premier_profile_pdf:
                self.attach_file(report_id, "premier_profile.pdf", "premier_profile.pdf", premier_profile_pdf)

        return {"report": "Business Report", "success": resp.ok, "resp": resp.text}


    def update_exp_personal_credit(self, account_id, account_name, bop_report, bop_report_pdf):
        response = bop_report.get("response_body")

        request_id = response.get("requestId")
        results = response.get("results", [])
        result = results[0] if len(results) and isinstance(results[0], dict) else {}

        consumer_identity = result.get("consumerIdentity", []) or []
        consumer_identity = consumer_identity[0] if len(consumer_identity) else {}

        tradelines = result.get("tradelines", []) or []
        tradeline_open_dates = filter(None, map(lambda tradeline: tradeline.get("openDate"), tradelines))
        tradeline_open_dates = list(map(lambda date: datetime.strptime(date, "%Y-%m-%d"), tradeline_open_dates))
        oldest_tradeline = min(tradeline_open_dates).strftime("%Y-%m-%d") if len(tradeline_open_dates) else None

        #***Begin Summing up the tradelines
        monthly_payment_amt_sum = 0
        status_code_97_cntr = 0
        status_code_93_cntr = 0

        for j in range(len(tradelines)):
            bop_report_tradeline_status = tradelines[j]
            status_code = bop_report_tradeline_status['status']['code']
        
            monthly_payment_amt = bop_report_tradeline_status['monthlyPaymentAmount']
        
            if monthly_payment_amt is not None:
                monthly_payment_amt_sum = monthly_payment_amt_sum + monthly_payment_amt
        
            if status_code == '93':
                status_code_93_cntr = status_code_93_cntr + 1
        
            elif status_code == '97':
                status_code_97_cntr = status_code_97_cntr +1

        print("Sum of monthly payment amounts: " + str(monthly_payment_amt_sum))
        print("Status code 93 count: " + str(status_code_93_cntr))
        print("Status code 97 count: " + str(status_code_97_cntr))
        #***End Summing up the tradelines

        profile_summary = result.get("profileSummary", {}) or {}

        risk_model = result.get("riskModel", []) or []
        risk_model = risk_model[0] if len(risk_model) else {}

        body = {
            "Experian_API_Payload__c": json.dumps(bop_report.get("request_body")),
            "Experian_API_Error_Response__c": json.dumps(bop_report.get("response_body")) if not result else "No error.",
            "AccountId__c": account_id,
            "Request_ID__c": request_id,
            "Name": "{} - Personal Credit Report ({})".format(account_name, datetime.utcnow().strftime("%Y%m%d-%H%M%S")),
            # "Name": consumer_identity.get("fullName") or "null",
            "Date_of_Credit_Report__c": datetime.now().strftime("%Y-%m-%d"),
            "Oldest_Tradeline__c": oldest_tradeline or "null",
            "Revolving_Balance__c": profile_summary.get("revolvingBalance") or 0,
            "Real_Estate_Balance__c": profile_summary.get("realEstateBalance") or 0,
            "Installment_Balance__c": profile_summary.get("installmentBalance") or 0,
            "Sum_of_Monthly_Personal_Debt__c": monthly_payment_amt_sum or 0,
            "Real_Estate_Payment__c": profile_summary.get("realEstatePayment") or 0,
            # "Installment_Payment__c": ,
            "Revolving_Payment__c": profile_summary.get("monthlyPayment") or 0, # Not sure about this
            "Credit_Score__c": risk_model.get("score"),
            "Score_Factor__c": (", ".join(risk_model.get("scoreFactors")) if risk_model.get("scoreFactors") is not None and len(risk_model.get("scoreFactors")) else None) or "null",
            # "Bankruptcy__c": ,
            "Public_Records__c": (", ".join([x.get('status', {}).get('definition') for x in result.get("publicRecord")]) if result.get("publicRecord") is not None and len(result.get("publicRecord")) else None) or "null",
            "Charge_Off_Accounts__c": status_code_97_cntr or 0,
            "Collection_Accounts__c": status_code_93_cntr or 0,
            "X30_Day__c": profile_summary.get("delinquenciesOver30Days") or 0,
            "X60_Day__c": profile_summary.get("delinquenciesOver60Days") or 0,
            "X90_Day__c": profile_summary.get("delinquenciesOver90Days") or 0,
            "Total_Inquiries__c": profile_summary.get("totalInquiries") or 0,
            "Total_Inquiries_Last_6_Months__c": profile_summary.get("inquiriesDuringLast6Months") or 0,
            "Total_Trade_Items__c": profile_summary.get("totalTradeItems") or 0,
            "Paid_Accounts__c": profile_summary.get("paidAccounts") or 0,
            "Satisfactory_Accounts__c": profile_summary.get("satisfactoryAccounts") or 0,
            "Now_Delinquent__c": profile_summary.get("nowDelinquentDerog") or 0,
            "Was_Delinquent__c": profile_summary.get("wasDelinquentDerog") or 0,

            "Disputed_Accounts_Excluded__c": profile_summary.get("disputedAccountsExcluded") or 0,
            "Public_Records_Count__c": profile_summary.get("publicRecordsCount") or 0,
            "Past_Due_Amount__c": profile_summary.get("pastDueAmount") or 0,
            "Monthly_Payment__c": profile_summary.get("monthlyPayment") or 0,

            "Monthly_Payment_Partial__c": (profile_summary.get("monthlyPaymentPartial") or {}).get("definition") or "null",
            "Real_Estate_Payment_Partial__c": (profile_summary.get("realEstatePaymentPartial") or {}).get("definition") or "null",
            "Revolving_Available_Percent__c": profile_summary.get("revolvingAvailablePercent") or 0,
            "Revolving_Available_Partial__c": (profile_summary.get("revolvingAvailablePartial") or {}).get("definition") or "null",

            "Oldest_Trade_Open_Date__c": profile_summary.get("oldestTradeOpenDate"),
            "Delinquencies_Over_30_Days__c": profile_summary.get("delinquenciesOver30Days") or 0,
            "Delinquencies_Over_60_Days__c": profile_summary.get("delinquenciesOver60Days") or 0,
            "Delinquencies_Over_90_Days__c": profile_summary.get("delinquenciesOver90Days") or 0,
            "Derog_Counter__c": profile_summary.get("derogCounter") or 0,
        }
        # body = { key: (value if value is not None else "null") for key, value in body.items() }

        url = "{}/services/data/v39.0/sobjects/Experian_Personal_Credit_Report__c".format(self.base_url)
        resp = requests.post(url, headers=self.default_headers, json=body)
        if resp.ok:
            report_id = resp.json()["id"]
            self.attach_file(report_id, "personal_credit_report.json", "personal_credit_report.json",
                             self.b64encode(json.dumps(bop_report, indent=4)))

            if bop_report_pdf:
                self.attach_file(report_id, "bop_report.pdf", "bop_report.pdf", bop_report_pdf)

        return {"report": "Personal Report", "success": resp.ok, "resp": resp.text}

    def get_exp_business_report(self, account_id):
        self.authenticate()

        soql = "SELECT+Id+FROM+Experian_Business_Report__c+WHERE+AccountId__c='{}'".format(account_id)
        url = "{}/services/data/v39.0/query/?q={}".format(self.base_url, soql)

        resp = requests.get(url, headers=self.default_headers)
        records = resp.json().get("records", [])

        return records[-1].get("Id") if len(records) else None

    def get_exp_personal_credit_report(self, account_id):
        self.authenticate()

        soql = "SELECT+Id+FROM+Experian_Personal_Credit_Report__c+WHERE+AccountId__c='{}'".format(account_id)
        url = "{}/services/data/v39.0/query/?q={}".format(self.base_url, soql)

        resp = requests.get(url, headers=self.default_headers)
        records = resp.json().get("records", [])

        return records[-1].get("Id") if len(records) else None

    def get_all_accounts(self, new_only=False, limit=None):
        # https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/dome_query.htm
        self.authenticate()

        soql = "SELECT+Id,RecordType.Name+FROM+Account"
        if new_only:
            soql = "{}+WHERE+credit_report_generated__c=false".format(soql)
        if limit is not None:
            soql = "{}+LIMIT+{}".format(soql, limit)

        url = "{}/services/data/v39.0/query/?q={}".format(self.base_url, soql)

        resp = requests.get(url, headers=self.default_headers)
        return resp.json()


    def b64encode(self, content):
        return base64.b64encode(content.encode("UTF-8")).decode()


    def get_business_owners(self, account_id):
        self.authenticate()

        query = "SELECT Id, Role__c FROM Contact WHERE AccountId = '{}'".format(account_id)
        url = "{}/services/data/v39.0/query/?q={}".format(self.base_url, quote_plus(query))

        resp = requests.get(url, headers=self.default_headers)
        contacts = resp.json().get("records", [])

        if len(contacts):
            business_owners = filter(
                lambda contact: contact.get("Role__c") is not None and "owner" in contact.get("Role__c").lower(),
                contacts
            )

            business_owners_info = []
            for owner in business_owners:
                contact_info = self.get_contact_by_id(owner.get("Id"))
                business_owners_info.append(contact_info)

            return business_owners_info

        return []


    def set_sf_credentials(self):
        aws_secret_id = os.environ["AWS_SECRET_ID"]
        aws_access_key_id = os.environ["AWS_ACCESS_KEY_ID"]
        aws_secret_access_key = os.environ["AWS_SECRET_ACCESS_KEY"]

        aws_session = boto3.session.Session()
        aws_secret_manager = aws_session.client(
            service_name="secretsmanager",
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
            region_name="us-west-2"
        )

        response = aws_secret_manager.get_secret_value(SecretId=aws_secret_id)
        secret = json.loads(response["SecretString"])

        self.user_name = secret["username"]
        self.password = secret["password"]
        self.security_token = secret["security_token"]

        self.client_id = secret["client_id"]
        self.client_secret = secret["client_secret"]
        self.auth_url = secret["auth_url"]


def years_ago(date):
    return relativedelta(datetime.now(), datetime.strptime(date, "%Y-%m-%d")).years


def format_address(addr_obj):
    if addr_obj:
        return ", ".join(filter(None, [
            addr_obj.get("street"),
            addr_obj.get("city"),
            addr_obj.get("state"),
            addr_obj.get("zip") + (addr_obj.get("zipExtension") or "") if addr_obj.get("zip") else None,
        ]))

if __name__ == '__main__':
    ctr = SalesForceAPI()
    # json_print(ctr.get_contact_by_id("003f400001KU2UcAAL"))
    json_print(ctr.get_business_owners("001f400001GYd8XAAT"))
    # json_print(ctr.get_account_by_id("001f400001HP4wyAAD"))
    # json_print(ctr.get_account_by_id("001Q000001HRG7JIAX"))
    # json_print(ctr.update_account_fields("001Q000001HRG7JIAX", {'test_field__c': 'test_data'}))
    # json_print(ctr.get_account_contact_role_summary())
    # json_print(ctr.attach_object("001f400000rqkrkAAA", "test_data.json", b'{"test": "test"}'))
    # json_print(ctr.get_attachment("00PQ0000008gYxAMAU"))
    # json_print(ctr.attach_file("001f400000rqkrkAAA", "test_file.txt", "test_file.txt", bytes("Test Content...", 'utf8')))
    # json_print(ctr.get_contact_ids_by_account_id("001Q000001HRG7JIAX", limit=1))
