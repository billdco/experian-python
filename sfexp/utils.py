import json


def json_print(obj):
    print(json.dumps(obj, indent=4))
