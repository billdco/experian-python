import requests
import time
from datetime import datetime
from sfexp.utils import json_print

class ProductionConfig:
    CLIENT_ID = "HXzFbG0yjE8rhvkDn9PYa7t28VnxOlRp"
    CLIENT_SECRET = "pmtNjq9rOJXvWeHl"
    USER_NAME = "api.billd"
    PASSWORD = "LakeTravis512"
    BASE_URL = "https://us-api.experian.com/"
    SUB_CODE = ["0553124", "0226920", "0313110", "0313190"][0]

class DevelopmentConfig:
    CLIENT_ID = "afDGVQc6TJ28loyEqLiL5VE3qIX3ulmm"
    CLIENT_SECRET = "CB40AfvVkoBO2mJP"
    USER_NAME = "manasa@billdco.com"
    PASSWORD = "LakeTravis26022@"
    BASE_URL = "https://sandbox-us-api.experian.com/"
    SUB_CODE = "0517635"


class ExperianAPI:
    def __init__(self):
        self.auth_token = ""

        config = DevelopmentConfig
        #config = ProductionConfig

        self.client_id = config.CLIENT_ID
        self.client_secret = config.CLIENT_SECRET
        self.user_name = config.USER_NAME
        self.password = config.PASSWORD
        self.base_url = config.BASE_URL

        self.sub_code = config.SUB_CODE

        self.default_headers = None

        self.authenticated_at = None
        self.token_expiry = 300 # estimated auth token expiry time (in seconds).

        # TODO: Only authenticate as needed
        # self.authenticate()

    def authenticate(self):
        if self.authenticated_at and self.token_expiry > (time.time() - self.authenticated_at):
            return

        url = "{}/oauth2/v1/token".format(self.base_url)
        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "grant_type": "password"
        }
        body = {
            "username": self.user_name,
            "password": self.password,
            "client_id": self.client_id,
            "client_secret": self.client_secret
        }
        resp = requests.post(url, headers=headers, json=body)
        resp_json = resp.json()
        print(resp_json)
        self.auth_token = resp_json.get('access_token')
        self.default_headers = {
            "Authorization": "Bearer " + self.auth_token,
            "Content-Type": "application/json"
        }

        self.authenticated_at = time.time()

    def financial_summaries(self, bin_id):
        self.authenticate()
        # https://developer.experian.com/sbcs/apis/post/v1/financialsummaries
        url = "{}/businessinformation/sbcs/v1/financialsummaries".format(self.base_url)
        body = {
            "bin": bin_id,
            "subcode": self.sub_code,
        }
        resp = requests.post(url, headers=self.default_headers, json=body)
        return resp.json()

    def small_business_search(self, name, street, city, state, zip_code, phone, tax_id, geo=False):
        self.authenticate()
        # https://developer.experian.com/sbcs/apis/post/v1/search
        url = "{}/businessinformation/sbcs/v1/search".format(self.base_url)
        body = {
            "name": name,
            "street": street,
            "city": city,
            "state": state,
            "zip": zip_code,
            "phone": phone,
            "taxId": tax_id,
            "geo": geo,
            # "comments": "testing",
            "subcode": self.sub_code
        }
        print('Searching for', body)
        resp = requests.post(url, headers=self.default_headers, json=body)
        return resp.json()

    def business_search(self, name, street, city, state, zip_code, phone, tax_id, geo=False):
        self.authenticate()
        # https://developer.experian.com/sbcs/apis/post/v1/search
        url = "{}/businessinformation/businesses/v1/search".format(self.base_url)
        body = {
            "name": name,
            "street": street,
            "city": city,
            "state": state,
            "zip": zip_code,
            "phone": phone,
            "taxId": tax_id,
            "geo": geo,
            # "comments": "testing",
            "subcode": self.sub_code
        }
        print('Searching for', body)
        resp = requests.post(url, headers=self.default_headers, json=body)
        return resp.json()

    def business_search(self, name, street, city, state, zip_code, phone, tax_id, geo=False):
        self.authenticate()
        # https://developer.experian.com/sbcs/apis/post/v1/search
        url = "{}/businessinformation/businesses/v1/search".format(self.base_url)
        body = {
            "name": name,
            "street": street,
            "city": city,
            "state": state,
            "zip": zip_code,
            "phone": phone,
            "taxId": tax_id,
            "geo": geo,
            # "comments": "testing",
            "subcode": self.sub_code
        }
        print('Searching for', body)
        resp = requests.post(url, headers=self.default_headers, json=body)
        return resp.json()

    def small_business_credit_scores(self, bin_id):
        self.authenticate()
        url = "{}/businessinformation/sbcs/v1/financialsummaries".format(self.base_url)
        body = {
            "bin": bin_id,
            "subcode": self.sub_code,
        }
        resp = requests.post(url, headers=self.default_headers, json=body)
        return resp.json()

    def business_commercial_scores(self, bin_id):
        self.authenticate()
        url = "{}/businessinformation/businesses/v1/scores".format(self.base_url)
        body = {
            "fsrScore": True,
            "commercialScore": True,
            "bin": bin_id,
            "subcode": self.sub_code,
        }
        resp = requests.post(url, headers=self.default_headers, json=body)
        return resp.json()

    def premier_profiles(self, bin_id):
        self.authenticate()
        url = "{}/businessinformation/businesses/v1/reports/premierprofiles".format(self.base_url)
        body = {
            "bin": bin_id,
            "subcode": self.sub_code,
        }
        resp = requests.post(url, headers=self.default_headers, json=body)
        return resp.json()

    def premier_profile_pdf(self, bin_id):
        self.authenticate()
        url = "{}/businessinformation/businesses/v1/reports/premierprofiles/pdf".format(self.base_url)
        body = {
            "bin": bin_id,
            "subcode": self.sub_code,
        }

        headers = { k: v for k, v in self.default_headers.items() }
        headers["Accept-Encoding"] = "gzip, deflate"

        resp = requests.post(url, headers=headers, json=body, stream=True, timeout=None)
        resp.raise_for_status()
        return resp.json()["results"] # base64encoded content

    def consumer_credit_report(self):
        self.authenticate()
        # https://developer.experian.com/consumer-credit-profile/apis/post/v2/credit-report
        url = "{}/consumerservices/credit-profile/v2/credit-report".format(self.base_url)
        body = {
            "creditProfile": {
                "subscriber": {
                    "preamble": "TEST",
                    "subscriberCode": "5991764"
                },
                "primaryApplicant": {
                    "name": {
                        "surname": "CONSUMER",
                        "firstName": "JONATHAN",
                        "middleName": "",
                        "generationCode": ""
                    },
                    "ssn": "999999990",
                    "dob": "1985"
                },
                "address": {
                    "currentAddress": {
                        "street": "10655 NORTH BIRCH STREET",
                        "city": "BURBANK",
                        "state": "CA",
                        "zipCode": "91502"
                    }
                },
                "otherInformation": {
                    "referenceNumber": "CR API",
                    "permissiblePurposeType": {
                        "type": "",
                        "terms": "",
                        "abbreviatedAmount": ""
                    },
                    "paymentHistory84": "N"
                },
                "addOns": {
                    "directCheck": "",
                    "demographics": {
                        "demographicsAll": "N",
                        "demographicsPhone": "N",
                        "demographicsGeoCode": "N"
                    },
                    "riskModels": {
                        "modelIndicator": [
                            "F", "3", "B"
                        ],
                        "scorePercentile": "",
                        "profileSummary": "Y",
                        "fraudShield": "Y",
                        "mla": "",
                        "ofac": "",
                        "ofacmsg": "",
                        "staggSelect": "",
                        "uniqueConsumerIdentifier": {
                            "getUniqueConsumerIdentifier": ""
                        }
                    },
                    "options": {"optionId": [""]}
                }
            }
        }
        resp = requests.post(url, headers=self.default_headers, json=body)
        return resp.json()

    def commerical_credit_status(self, bin_id):
        self.authenticate()
        # https://developer.experian.com/businesses/apis/post/v1/creditstatus
        url = "{}/businessinformation/businesses/v1/creditstatus".format(self.base_url)
        body = {
            "bin": bin_id,
            "subcode": self.sub_code,
        }
        resp = requests.post(url, headers=self.default_headers, json=body)
        return resp.json()

    def get_business_owner_profile(self, **kwargs):
        # https://developer.experian.com/business-owners/apis/post/v1/reports/bop
        self.authenticate()
        url = "{}/businessinformation/businessowners/v1/reports/bop".format(self.base_url)
        business_owner = {
            "ownerName": {
                "lastName": kwargs.get("last_name"),
                "firstName": kwargs.get("first_name"),
                "middleName": kwargs.get("middle_name"),
                "generationCode": kwargs.get("suffix")
            },
            "ssn": kwargs.get("ssn") if kwargs.get("ssn") != "XXX-XX-0000" else None,
            "currentAddress": {
                "street": kwargs.get("street"),
                "city": kwargs.get("city"),
                "state": kwargs.get("state"),
                "zip": kwargs.get("zip")
            },
            # "title": kwargs.get("role")
        }

        if kwargs.get("dob"):
            birth_year, birth_month, birth_day = split_date(kwargs.get("dob"))
            business_owner["dob"] = {
                "day": birth_day,
                "month": birth_month,
                "year": birth_year
            }

        body = {
            "subcode": self.sub_code,
            "businessOwners": [
                business_owner
            ]
        }

        resp = requests.post(url, headers=self.default_headers, json=body)
        return {
            "request_body": body,
            "response_body": resp.json()
        }


    def get_business_owner_profile_pdf(self, **kwargs):
        # https://developer.experian.com/business-owners/apis/post/v1/reports/bop/pdf
        self.authenticate()
        url = "{}/businessinformation/businessowners/v1/reports/bop/pdf".format(self.base_url)
        business_owner = {
            "ownerName": {
                "lastName": kwargs.get("last_name"),
                "firstName": kwargs.get("first_name"),
                "middleName": kwargs.get("middle_name"),
                "generationCode": kwargs.get("suffix")
            },
            "ssn": kwargs.get("ssn") if kwargs.get("ssn") != "XXX-XX-0000" else None,
            "currentAddress": {
                "street": kwargs.get("street"),
                "city": kwargs.get("city"),
                "state": kwargs.get("state"),
                "zip": kwargs.get("zip")
            },
            # "title": kwargs.get("role")
        }

        if kwargs.get("dob"):
            birth_year, birth_month, birth_day = split_date(kwargs.get("dob"))
            business_owner["dob"] = {
                "day": birth_day,
                "month": birth_month,
                "year": birth_year
            }

        body = {
            "subcode": self.sub_code,
            "businessOwners": [
                business_owner
            ]
        }

        resp = requests.post(url, headers=self.default_headers, json=body, timeout=None)
        if resp.ok:
            result = resp.json()
            if result["success"] is True:
                return result["results"]


def split_date(raw_date):
    if not raw_date:
        return None, None, None
    date = datetime.strptime(raw_date, "%Y-%m-%d")
    return date.year, date.month, date.day


if __name__ == '__main__':
    ctr = ExperianAPI()
    json_print(ctr.small_business_search("Experian", "535 ANTON BLVD", "Costa Mesa", "CA", "92626", "9495673800", "176970333"))
    # json_print(ctr.consumer_credit_report())
    # json_print(ctr.commerical_credit_status("796744203"))
    # json_print(ctr.business_commercial_scores("796744203"))
